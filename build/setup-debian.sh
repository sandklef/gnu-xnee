#!/bin/sh

DOC_PKGS="texlive-extra-utils texinfo imagemagick pkg-config dia  texi2html texlive-font-utils" 
#  gs-common
DEVEL_PKGS="libtool gcc automake autoconf make libgtk2.0-dev libxtst-dev"

my_install() 
{
    apt-get install -y  $*
    if [ $? -ne 0 ]
    then
	echo "Failed installing: $*"
	exit 1
    fi
}


my_install $DOC_PKGS

my_install $DEVEL_PKGS

